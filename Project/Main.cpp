#include <iostream>
#include <iomanip>

class Vector {
private:
	int _x{ 0 };
	int _y{ 0 };
	int _z{ 0 };
public:
	Vector() {}

	Vector(int x, int y, int z) : _x(x), _y(y), _z(z) { }

	void PrintCoordinates() {
		std::cout << "[x:" << _x << ", y:" << _y << ", z:" << _z << "]" << std::endl;
	}

	void PrintLength() {
		std::cout << std::fixed << std::setprecision(16) << std::sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)) << std::endl;
	}
};

void main() {
	Vector v{ 2,5,7 };
	v.PrintCoordinates();
	v.PrintLength();
}